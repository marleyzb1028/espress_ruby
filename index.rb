# index.rb
require 'sinatra'

get '/hello' do
    'Sinatra GET'
  end
  
  post '/hello' do
  'Sinatra POST'
  end
  
  patch '/hello' do
  'Sinatra PATCH'
  end
  
  delete '/hello' do
    'Sinatra DELETE'
  end
  
  put '/hello' do
    'Sinatra PUT'
  end
  